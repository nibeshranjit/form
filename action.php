<?php
include "config.php";
$query = "SELECT * FROM form ORDER BY ID ASC";
    $result = mysqli_query( $conn,$query);?>
    <DOCTYPE html>
<head>
    <title>Form Data</title>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
        
        <!-- Latest compiled and minified CSS -->
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous">
</script>  

 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<script src="./js/query.js"></script>

</head>
<body>

    <div class ="container">
        <div class="page-header">
          <h2 class="text-center" ><b>Table Form</b></h2>
        </div>
  
    <a href="create.php" class="btn btn-info" > CREATE </a><br><br>
 <input type='hidden' id='sort' value='asc'>
   
 <div class="table-responsive">
   
  <table class="table table-striped" id="empTable">                     
        <thead>
            <tr>
              <th><span onclick='sortTable("ID");'>ID</th>

              <th><span onclick='sortTable("firstname");'>First Name</th>
              
              <th><span onclick='sortTable("lastname");'>Last Name</th>

              <th><span onclick='sortTable("country");'>Country</th>
              
              <th><span onclick='sortTable("subject");'>Subject</th> 

              <th>Action</th>
            </tr>
        </thead>
        

<?php 
    
    
    while($row = mysqli_fetch_array($result)){
                     
      $ID = $row['ID'];//id is key $row['ID'] gives value of key id
      $firstname = $row['firstname'];
      $lastname = $row['lastname'];
      $country = $row['country'];
      $subject = $row['subject'];
      ?>
    <tr>
      <td><?php echo $ID; ?></td>
      <td><?php echo $firstname; ?></td>
      <td><?php echo $lastname; ?></td>
      <td><?php echo $country; ?></td>
      <td><?php echo $subject; ?></td>
      <td>
      <a href="edit.php?id=<?php echo $ID;?>" class="btn btn-success">EDIT</a>
      <a href="delete.php?id=<?php echo $ID;?>" class="btn btn-danger" id="demo" onclick="getConfirmation();">DELETE</a>  
    </td>
    </tr>
    <?php
    }
    ?>
  </table>
</div>
</body>
</html>
