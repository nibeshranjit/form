<?php 
include_once("config.php");
$user_id=$_GET['id'];
$query= "SELECT * from form WHERE ID='$user_id'";
//die($query);
$result= mysqli_query($conn, $query);
$data=mysqli_fetch_assoc($result);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>EDIT</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJsA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
 </head>
  <body>
  		<div class="container">
  			
  			<div class="page-header">
  		  <h2 class="text-center">Edit Form</h2>
  		</div>

  <form action="update.php" class="form" method="post"><!-- rememeber to include method --> 
  	<div class="form-group">

  		<input type="hidden" name="id" value="<?php echo $data['ID']; ?>" /> <br>
      </div>

<div class="form-group">

      <label for="firstname" >First Name:</label>
      <input type="text" class="form-control" id="fname" placeholder="Enter First Name" name="firstname" value="<?php echo $data['firstname'];?>">
    </div>

    <div class="form-group">
      <label for="lastname" >Last Name:</label>
      <input type="text" class="form-control" id="lname" placeholder="Enter Last Name" name="lastname" value="<?php echo $data['lastname'];?>"">
    </div>
    
    <div class="form-group">
      <label for="country">Country:</label>
      <input type="country" class="form-control" id="country" placeholder="Enter Your Country" name="country" value="<?php echo $data['country'];?>">
    </div>
    

    <div class="form-group">
      <label for="subject" >Subject:</label>
      <input type="text-area" class="form-control" id="subject" placeholder="Enter Subject" name="subject" value=<?php echo $data['subject'];?>>
    </div>
        
<input type="submit" value="submit" class="btn btn-primary">
<button type="button" button onclick="location.href='action.php'" class="btn btn-warning">CANCEL</button>
  </form>
</div>

</body>
</html>
